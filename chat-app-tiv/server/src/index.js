const http = require('http')
const express = require('express')
const socketio = require('socket.io')
const Filter = require('bad-words')

const {
  addUser,
  getUsers,
  addRoom,
  getUser,
  getRoom,
  addUserInRoom,
  getUsersInRoom,
  getMessagesInARoom,
  addMessage
} = require("./database").dbMethods;

const app = express()
const server = http.createServer(app)
const io = socketio(server)

const port = process.env.PORT || 8080

const generateMessage = (username, text) => {
  return {
    username,
    text,
    createdAt: new Date().getTime()
  };
};

io.on('connection', async (socket) => {
    console.log('New WebSocket connection')
    try {
          socket.on("signup", (options, cb) => {
            addUser({ ...options })
              .then(() => {
                socket.emit("log", { error: false, message: "Success!" });
                getUser({ ...options }).then(user => {
                  socket.emit("userData", user);
                });
              })
              .catch(e => {
                console.log(e);
                socket.emit("log", { error: true, message: "Failed!" });
              });
          });
          socket.on("Get Users", () => {
            getUsers().then(users => {
              socket.emit("users", users);
            });
          });
          socket.on("login", payload => {
            getUser({ ...payload }).then(user => {
              socket.emit("userData", user);
            });
          });

          socket.on('join', async ({roomName,user}) => {
              let room = await getRoom(roomName)
              console.log('Room: ',room)
              if(room.roomName !== ''){
                await addUserInRoom(user.id,room.id)
                await addMessage(user.id,room.id,'Hi, This is '+user.username)
                  socket.join(roomName)
                  socket.emit('message', generateMessage('Admin', 'Welcome!'))
                  socket.broadcast.to(roomName).emit('message', generateMessage('Admin', `${user.username} has joined!`))
                  let users = await getUsersInRoom(room.id);
                  let messages = await getMessagesInARoom(room.id)
                  io.to(roomName).emit("roomData", {
                    room: roomName,
                    users,
                    messages
                  });
              } else {
                  console.log('Adding room: ',roomName)
                  await addRoom({roomName})
                    let room = await getRoom(roomName);

                await addMessage(user.id,room.id,'Hi, This is '+user.username)
                  await addUserInRoom(user.id, room.id);
                  socket.join(roomName);
                  socket.emit("message", generateMessage("Admin", "Welcome!"));
                  socket.broadcast
                    .to(roomName)
                    .emit(
                      "message",
                      generateMessage("Admin", `${user.username} has joined!`)
                    );
                let users = await getUsersInRoom(room.id)
                let messages  = await getMessagesInARoom(room.id)
                  io.to(roomName).emit("roomData", {
                    room: roomName,
                    users: users,
                    messages
                  });
              }
          })
        } catch(err) {
        console.log(err)
    }

    socket.on('sendMessage',async  ({user,roomName,message}) => {
              let room = await getRoom(roomName);
        const filter = new Filter()
       await addMessage(user.id,room.id,message)
        if (filter.isProfane(message)) {
            socket.emit('log', {error:true,message:'Profanity is not allowed'})
        }
        io.to(room.roomName).emit('message', generateMessage(user.username, message))
        let users = await getUsersInRoom(room.id);
        let messages = await getMessagesInARoom(room.id);
        io.to(roomName).emit("roomData", {
          room: roomName,
          users: users,
          messages
        });
    })
})

server.listen(port, () => {
    console.log(`Server is up on port ${port}!`)
})