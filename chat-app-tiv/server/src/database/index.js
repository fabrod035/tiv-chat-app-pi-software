var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database("db");

db.run(`CREATE TABLE IF NOT EXISTS users (
    id INTEGER PRIMARY KEY autoincrement,
    username TEXT NOT NULL,
    password TEXT NOT NULL
)`);

db.run(`CREATE TABLE IF NOT EXISTS rooms (
    id INTEGER PRIMARY KEY autoincrement,
    roomName TEXT NOT NULL
)`);

db.run(`CREATE TABLE IF NOT EXISTS user_room (
    id INTEGER PRIMARY KEY autoincrement,
    user_id INTEGER NOT NULL,
    room_id INTEGER NOT NULL
)`);

db.run(`CREATE TABLE IF NOT EXISTS messages (
    id INTEGER PRIMARY KEY autoincrement,
    user_id INTEGER NOT NULL,
    room_id INTEGER NOT NULL,
    message TEXT NOT NULL
)`);

function addUser({username,password}){
    return new Promise((res,rej) => {
        db.get('insert into users("username","password") values (?, ?)', [username,password],(err,row) => {
            if(err){
                return rej(err)
            }
            return res(row)
        })
    })
}

function addRoom({roomName}){
    return new Promise((res,rej) => {
        db.get('insert into rooms ("roomName") values(?)',[roomName],(err,row) => {
            console.log(err)
            if(err){
                return rej(err)
            }
            return res(row)
        })
    })
}

function addUserInRoom(user_id,room_id){
    return new Promise((res,rej) => {
        db.run('insert into user_room("user_id","room_id") values (?,?)',[user_id,room_id],(err,row) => {
            if(err){
                return rej(err)
            }
            return res(row)
        })
    })
}

function getUsers(){
    return new Promise((res,rej) => {
        db.all('select * from users',(err,rows) => {
            res(rows)
        })
    })
}

function getUser({username,password}){
    return new Promise((res,rej) => {
        db.all('select * from users where username = ? and password = ? ',[username,password],(err,row) => {
            if(err){
                return rej(err)
            }
            return res(row)
        })
    })
}
function getRoom(roomName) {
  return new Promise((res, rej) => {
    db.all(
      "select * from rooms",
      (err, rows) => {
        if (err) {
          return rej(err);
        }
        //console.log(rows)
        let room = rows.find(row => row.roomName===roomName)
        return res(room?room:{id:'',roomName:''});
      }
    );
  });
}

function getUsersInRoom(roomId){
    return new Promise((res,rej) => {
        db.all('select * from user_room where room_id = ?',[roomId] ,(err,rows) => {
            if(err){
             return rej(err)
            }
            console.log(rows)
            res(rows)
        })
    })
}

async function getMessagesInARoom(room_id){
    return new Promise((res,rej) => {
        db.all('select * from messages',(err,rows) => {
            if(err){
                console.log('******',err.message)
                return rej(err)
            }
            let messages = rows.filter(row => Number(row.room_id) === Number(room_id))
            console.log(messages);
            res(messages);
        })
    })
}

async function addMessage(user_id,room_id,message){
    return new Promise((res,rej) => {
        db.all('insert into messages("user_id","room_id","message") values (?,?,?)',[user_id,room_id,message],(err,row) => {
            if(err){
               return rej(err)
            }
            res(true)
        })

    })
}


module.exports.dbMethods = {
  addUser,
  getUsers,
  addRoom,
  addUserInRoom,
  getUser,
  getRoom,
  getUsersInRoom,
  getMessagesInARoom,
  addMessage
};
module.exports.db = db