import React,{useState,useEffect} from 'react'
import {Row,Col} from 'reactstrap'
import Room from './Room'
import Inbox from './components/Inbox'
import Box from './Box'
export default function ChatBox({user,socket}){
    const [roomName,setRoomName] = useState('')
    const [roomState,setRoomState] = useState([])
    
    useEffect(() => {
        socket.on("roomData", ({ room, users,messages }) => {
          setRoomState(users);
          //console.log({room,users,messages})
        });
    })
    const onSubmit = roomName => {
        return e => {
            e.preventDefault()
            setRoomName(roomName)
            if(roomName){
                //console.log('Sending Join: ')
                socket.emit("join", { roomName, user });
            }
            
        }
    }
    return (
      <Row>
        <Col>
          {roomName ? (
            <Inbox
              currentUser={user}
              roomName={roomName}
              roomState={roomState}
              socket={socket}
            />
          ) : (
            <Room onSubmit={onSubmit} />
          )}
        </Col>
      </Row>
    );
}

/*
{/* <Box
              currentUser={user}
              roomName={roomName}
              roomState={roomState}
              socket={socket}
            /> */
