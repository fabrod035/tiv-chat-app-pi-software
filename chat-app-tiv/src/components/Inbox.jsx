import React,{useState,useEffect,useRef} from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Paper from "@material-ui/core/Paper";
import Fab from "@material-ui/core/Fab";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import MenuIcon from "@material-ui/icons/Menu";
import AddIcon from "@material-ui/icons/Add";
import SearchIcon from "@material-ui/icons/Search";
import MoreIcon from "@material-ui/icons/MoreVert";

import {Form,FormGroup,Input} from 'reactstrap'

const messages = [
  {
    id: 1,
    primary: "Brunch this week?",
    secondary:
      "I'll be in the neighbourhood this week. Let's grab a bite to eat",
    person: "/static/images/avatar/5.jpg"
  }
];

const useStyles = makeStyles(theme => ({
  text: {
    padding: theme.spacing(2, 2, 0)
  },
  paper: {
    paddingBottom: 50
  },
  list: {
    marginBottom: theme.spacing(2)
  },
  subheader: {
    backgroundColor: theme.palette.background.paper
  },
  appBar: {
    top: "auto",
    bottom: 0
  },
  grow: {
    flexGrow: 1
  },
  fabButton: {
    position: "absolute",
    zIndex: 1,
    top: -30,
    left: 0,
    right: 0,
    margin: "0 auto"
  }
}));

export default function BottomAppBar({currentUser,roomName,roomState,socket}) {
  const classes = useStyles();
const [message, setMessage] = useState("");
const [messages, setMessages] = useState([]);
const ref = useRef();

useEffect(() => {
  ref.current.scrollTop = ref.current.scrollHeight;
  socket.on("message", data => {
    //console.log('Data: ',data)
  });
  console.log('Rendered Inbox')
  socket.on("roomData", ({ room, users, messages }) => {
    //setRoomState(users);
    //console.log({ room, users, messages });
    let transform = messages.map(v => ({id:v.id,primary:v.message}))
    setMessages(transform);
  });
},[messages]);
const send = e => {
  e.preventDefault();
  setMessage("");
  socket.emit("sendMessage", { user: currentUser, roomName, message });
};

  return (
    <React.Fragment>
      <CssBaseline />
      <Paper square className={classes.paper}>
        <Typography className={classes.text} variant="h5" gutterBottom>
          Room Name: {roomName}
        </Typography>
        <List className={classes.list} ref={ref}>
          {messages.map(({ id, primary, secondary, person }) => (
            <React.Fragment key={id}>
              {id === 1 && (
                <ListSubheader className={classes.subheader}>
                  Today
                </ListSubheader>
              )}
              {id === 3 && (
                <ListSubheader className={classes.subheader}>
                  Yesterday
                </ListSubheader>
              )}
              <ListItem button>
                <ListItemAvatar>
                  <Avatar alt="Profile Picture" src={person} />
                </ListItemAvatar>
                <ListItemText primary={primary} secondary={secondary} />
              </ListItem>
            </React.Fragment>
          ))}
          <ListItem>
            {/* <Box m={1}>
              <TextField required id="standard-required" label="Required" />
              <Button variant="contained" color="secondary">
                Secondary
              </Button>
            </Box> */}
            <Form onSubmit={send}>
              <FormGroup>
                <Input
                  type="text"
                  onChange={e => setMessage(e.target.value)}
                  name="message"
                  value={message}
                  placeholder="Enter message"
                />
              </FormGroup>
              <Button type="submit" variant="contained" color="secondary">Send</Button>
            </Form>
          </ListItem>
        </List>
      </Paper>
    </React.Fragment>
  );
}
