import React,{useReducer,useEffect,useState} from 'react';
import {toast,ToastContainer} from 'react-toastify'
import {Row,Col,Container} from 'reactstrap'
import Authentication from './Authentication'
import Header from './components/Header'
import Signin from './components/Signin'
import Signup from './components/Signup'
import ChatBox from './ChatBox';
import Box from '@material-ui/core/Box'
function App({socket}) {
  const [isSignin,setIsSignin] = useState(true)
  const [user,setUser] = useReducer(
    (state,newState)=>({...state,...newState}),
    {id:'',username:''}
  )
  useEffect(() => {
    socket.on('users',users => {
      //console.log('Users:',users)
    })
    socket.on('userData', user => {
      //console.log('In DB: ',user)
      if(user.length === 0){
        toast.error('Wrong username or password!')
      }
      //window.localStorage.setItem('user',user[0]);
      setUser(user[0])
    })
    socket.on('log',response => {
      if(response.error){
        return toast.error(response.message)
      }
      toast.success(response.message)
    })
  },[user.id,socket])
  const onSubmit = payload => {
    return e => {
      e.preventDefault()
      if(payload.username.trim() === '' || payload.password.trim()===''){
        return toast.error("User/Pass can't be empty");
      }
      socket.emit('signup', payload)
    }
  }
  const getUsers = () => {
    socket.emit('Get Users',d => {
      //console.log(d)
    })
  }
  const onLogin = (payload) => {
    return e => {
      e.preventDefault();
      socket.emit('login',payload)
    }
  }
  const toggleSigninSignup = (e) => {
    e.preventDefault();
    setIsSignin(!isSignin)
  }
  return (
    <React.Fragment>
      <Header />
      <Box m={1}>
        <Container>
          <Row>
            <Col>
              <ToastContainer />
              {!user.id ? (
                <Col>
                  <Row>
                    {isSignin ? (
                      <Col>
                        {/* <Authentication onSubmit={onSubmit} mode="Signup" /> */}
                        <Signin
                          onLogin={onLogin}
                          toggleSigninSignup={toggleSigninSignup}
                        />
                      </Col>
                    ) : (
                      <Col>
                        {/* <Authentication onSubmit={onLogin} mode="Login" /> */}
                        <Signup
                          onSubmit={onSubmit}
                          toggleSigninSignup={toggleSigninSignup}
                        />
                      </Col>
                    )}
                  </Row>
                </Col>
              ) : (
                <ChatBox user={user} socket={socket} />
              )}
              {/* <button onClick={getUsers}>Get</button> */}
            </Col>
          </Row>
        </Container>
      </Box>
    </React.Fragment>
  );
}

export default App;
