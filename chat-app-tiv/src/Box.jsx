import React,{useEffect,useState,useRef} from 'react'
import {Container,Row,Col,Form,FormGroup,Input,Button} from 'reactstrap'
import './Box.css'
export default function Box({currentUser,roomState,socket,roomName}){
    const [message,setMessage] = useState('')
    const [messages,setMessages] = useState([])
    const ref = useRef()
    
    useEffect(() => {
      ref.current.scrollTop = ref.current.scrollHeight;
        socket.on('message', data => {
            //console.log('Data: ',data)
        })
        socket.on("roomData", ({ room, users, messages }) => {
          //setRoomState(users);
          //console.log({ room, users, messages });
          setMessages(messages)
        });
    })
    const send = e => {
        e.preventDefault();
        setMessage('')
        socket.emit('sendMessage',{user:currentUser,roomName,message})
    }
    return (
      <Container>
        <h3>Room Name: {roomName}</h3>
        <div
          className="m-3"
          style={{
            overflow: "scroll",
            height: "50vh",
            border: "1px solid green"
          }}
          ref={ref}
        >
          {messages.map(message => (
            <div className="card" key={message.id}>
              <div className="card-body">
                <p className="font-italic">
                  <span>
                    <b>{message.user_id}: </b>
                  </span>
                  {message.message}
                </p>
              </div>
            </div>
          ))}
        </div>

        <Form onSubmit={send}>
          <FormGroup>
            <Input
              type="text"
              onChange={e => setMessage(e.target.value)}
              name="message"
              value={message}
              placeholder="Enter message"
            />
          </FormGroup>
          <Button type="submit">Send</Button>
        </Form>

        {/* <form onSubmit={send}>
          <input
            type="text"
            name="message"
            onChange={e => setMessage(e.target.value)}
          />
          <button type="submit">Send</button>
        </form> */}
      </Container>
    );
}