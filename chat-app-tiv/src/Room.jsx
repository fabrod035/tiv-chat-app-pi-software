import React,{useState} from 'react'
import {Form,FormGroup,Label,Input,Button} from 'reactstrap'
export default function Room({onSubmit}){
    const [room,setRoom] = useState('')
    return (

        <Form onSubmit={onSubmit(room)}>
      <FormGroup>
        <Label for="room">Room Name</Label>
        <Input type="text" onChange={e => setRoom(e.target.value)} name="roomName" id="room" placeholder="Enter room name" />
      </FormGroup>
      <Button onSubmit={onSubmit(room)}>Enter</Button>
      </Form>

)
}