import React,{useReducer} from 'react';
import {Form,FormGroup,Label,Input,Button} from 'reactstrap'
export default function Authentication({onSubmit,mode}){
    const [state, setState] = useReducer(
      (state, newState) => ({...state,...newState}),
      { username: "", password: "" }
    );
    const handleChange = e => {
        e.preventDefault()
        let {name,value} = e.target;
        setState({[name]:value})
    }
    return (
      <div>
      <h3 className="display-5">{mode}</h3>
        <Form onSubmit={onSubmit(state)}>
          <FormGroup>
            <Input
              type="text"
              onChange={handleChange}
              name="username"
              id="username"
              placeholder="Username"
            />
          </FormGroup>
          <FormGroup>
            <Input
              type="password"
              name="password"
              id="password"
              onChange={handleChange}
              placeholder="Password"
            />
          </FormGroup>
          <Button type="submit">{mode}</Button>
        </Form>
      </div>
    );
}
{/* <form onSubmit={onSubmit(state)}>
    <input onChange={handleChange} name="username"/>
    <input onChange={handleChange} name="password"/>
    <button type="submit">{mode}</button>
</form> */}